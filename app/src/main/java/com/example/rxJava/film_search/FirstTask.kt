package com.example.rxJava.film_search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.example.rxJava.R
import com.example.rxJava.store_changes.SecondTask
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_first_task.*
import java.util.concurrent.TimeUnit
import io.reactivex.subjects.PublishSubject

// Требуется реализовать механизм поиска фильмов. Пользователь вводит поисковую строку
// в текстовое поле, через 400 миллисекунд после окончания ввода запускается поиск.
// Поиск выполняется, если длина введенной строки больше и равна трем символам.
// Сначала пользователю отображается прогресс бар, сообщающий о том, что поиск начался.
// Затем через какое-то время мы получаем список найденных фильмов или ошибку.

class ResultSearch(val result: ArrayList<String>?,
                   val error: Throwable?,
                   val isStartLoading : Boolean = false)


class FirstTask : AppCompatActivity() {

    private var subject = PublishSubject.create<String>()
    private lateinit var subscription : Disposable
    private var count: Int = 0

    private val watcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(p0: Editable?) {
            subject.onNext(p0.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_task)
        input_film.addTextChangedListener(watcher)

        to_second_task.setOnClickListener {
            startActivity(Intent(this@FirstTask, SecondTask::class.java))
        }

        subscription = subject
            .debounce(400, TimeUnit.MILLISECONDS)
            .filter { it.length >= 3 }
            .switchMap {
                Single.create<ArrayList<String>> {
                    // get response from server
                    Thread.sleep(500)
                    it.onSuccess(ArrayList())
                }
                    .map { query -> ResultSearch(query, null) }
                    .onErrorReturn { error -> ResultSearch(null, error) }
                    .toObservable()
                    .startWith(ResultSearch(null, null, true))
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when {
                    it.isStartLoading -> startLoading()
                    it.result == null -> Toast.makeText(this, it.error?.message, Toast.LENGTH_LONG).show()
                    else -> showResults(++count)
                }
            }
    }

    private fun showResults(count: Int) {
        text_result.text = "Some data after loading \nIteration: $count"
        text_result.visibility = View.VISIBLE
        progress.visibility = View.GONE
    }

    private fun startLoading(){
        text_result.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        subscription.dispose()
    }
}
