package com.example.rxJava.store_changes

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rxJava.R
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_second_task.*
import java.lang.IllegalArgumentException
import kotlin.collections.ArrayList
import kotlin.random.Random

// Имеется каталог, в котором находятся изображения.
// Необходимо подписаться на изменения (добавление и удаление изображений)
// файлов в данном каталоге и отображать пользователю актуальный список изображений,
// отсортированный по алфавиту.

// элемент массива
class Image(val name: String)

// обертка для события изменения и состояния текущего массива файлов
class Result(var event: Event?, var array: ArrayList<Image>)

class SecondTask : AppCompatActivity() {

    private lateinit var subscription : Disposable
    private lateinit var imagesAdapter: ImagesAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_task)

        recycler_images.apply {
            imagesAdapter = ImagesAdapter(this@SecondTask, ArrayList())
            adapter = imagesAdapter
            layoutManager = LinearLayoutManager(this@SecondTask)
        }


        val delObservable = Observable.create<Event> {
            Thread() {
                val count = Random.nextInt(8)
                try {
                    for (i in 0 until count) {
                        it.onNext(Remove(i))
                        Thread.sleep(800)
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
                it.onComplete()
            }.start()
        }


        val addObservable = Observable.create<Event> {
            val images = getInitArray()

            Thread() {
                val count = Random.nextInt(8)
                for(i in 0 until count) {
                    it.onNext(Addition(images.get(i)))
                    Thread.sleep(800)
                }
                it.onComplete()
            }.start()
        }

        subscription = Single
            .just(getInitArray())
            .toObservable()
            .flatMap { Observable.mergeArray(delObservable, addObservable) }
            .map {
                // convert to Result type
                Result(it, getInitArray())
            }
            .scan { t1, t2 ->
                when {
                    t2.event is Remove -> t1.array.removeAt((t2.event as Remove).item)
                    t2.event is Addition -> t1.array.add((t2.event as Addition).item)
                    else -> throw IllegalArgumentException()
                }
                Result(null, t1.array)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                imagesAdapter.setArray(it.array)
                imagesAdapter.notifyDataSetChanged()
            }
    }

    private fun getInitArray(): ArrayList<Image> {
        val images = ArrayList<Image>()

        images.add(Image("first"))
        images.add(Image("second"))
        images.add(Image("third"))
        images.add(Image("fourth"))
        images.add(Image("fifth"))
        images.add(Image("sixth"))
        images.add(Image("seventh"))
        images.add(Image("eighth"))

        return images
    }

    override fun onDestroy() {
        super.onDestroy()
        subscription.dispose()
    }
}



