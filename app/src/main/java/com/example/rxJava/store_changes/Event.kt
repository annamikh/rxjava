package com.example.rxJava.store_changes

interface Event

class Remove(val item: Int) : Event
class Addition(val item: Image) : Event