package com.example.rxJava.store_changes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rxJava.R

class ImagesAdapter(
    private val context: Context,
    private var images: ArrayList<Image>
) : RecyclerView.Adapter<ImagesAdapter.ImageHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder =
        ImageHolder(
            LayoutInflater.from(context).inflate(
                R.layout.list_item_image,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.imageName.text = images[position].name
    }

    fun setArray(items: List<Image>){
        images = ArrayList(items)
    }

    class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageName : TextView = itemView.findViewById(R.id.image_name)
    }
}